# Corsa Benchmarks

To execute a benchmark (e.g. list_to_nat.exs):

```shell
$ mix run [benchmark_name]
```
