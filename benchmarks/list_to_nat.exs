defmodule Example do
  def list_to_nat(l), do: list_to_nat(l, 0)
  defp list_to_nat([], result), do: result
  defp list_to_nat([n | rest], acc), do: list_to_nat(rest, 10 * acc + n)
end

defmodule NotInstrumented do
  defdelegate list_to_nat(l), to: Example
end

# defmodule NotInstrumented do
#   defdelegate list_to_nat(l), to: Example
#   defoverridable list_to_nat: 1

#   def list_to_nat(l) do
#     super(l)
#   end
# end

defmodule TriviallyInstrumented do
  use Corsa

  @pre list_to_nat(l), do: true
  @post list_to_nat(l), do: true
  @throws list_to_nat(l), do: false
  defdelegate list_to_nat(l), to: Example
end

defmodule Instrumented do
  use Corsa

  @pre list_to_nat(l) do
    is_list(l) and not List.improper?(l) and is_decimal_list(l)
  end
  @post list_to_nat(l) do
    l == Integer.digits(result)
  end
  @throws list_to_nat(l) do
    not is_decimal_list(l)
  end

  defdelegate list_to_nat(l), to: Example

  defp is_decimal_list([n | rest]) do
    is_decimal(n) and n > 0 and Enum.all?(rest, &is_decimal/1)
  end

  defp is_decimal(n) do
    is_integer(n) and n >= 0 and n < 10
  end
end

generate_decimal_list = fn n ->
  for i <- 0..n, reduce: [] do
    acc when i == 0 -> acc
    acc -> [Enum.random(1..9) | acc]
  end
end

inputs =
  for n <- [100 | Enum.to_list(100..1000//100)] do
    {inspect(n), generate_decimal_list.(n)}
  end

Benchee.run(
  %{
    "not instrumented" => fn input -> NotInstrumented.list_to_nat(input) end,
    "trivially instrumented" => fn input -> TriviallyInstrumented.list_to_nat(input) end,
    "instrumented" => fn input -> Instrumented.list_to_nat(input) end
  },
  inputs: inputs,
  # parallel: 8,
  time: 1,
  formatters: [
    {Benchee.Formatters.CSV, file: "results/list_to_nat.csv"}
    # {Benchee.Formatters.HTML, file: "results/list_to_nat.html"},
    # {Benchee.Formatters.Console, file: "results/list_to_nat.txt"}
  ]
)
