# TODO: test and capture warnings
defmodule CorsaTest do
  import TestHelpers
  use ExUnit.Case
  import ExUnit.CaptureIO

  describe "syntax error" do
    test "@pre" do
      defmodule TestError do
        use Corsa
        @pre 1
      end
      |> test_error(Corsa.PreError, "syntax error in @pre")
    end

    test "@post" do
      defmodule TestError do
        use Corsa
        @post 1
      end
      |> test_error(Corsa.PostError, "syntax error in @post")
    end

    test "@throws" do
      defmodule TestError do
        use Corsa
        @throws 1
      end
      |> test_error(Corsa.ThrowsError, "syntax error in @throws")
    end

    test "@within" do
      defmodule TestError do
        use Corsa
        @within 1
      end
      |> test_error(Corsa.WithinError, "syntax error in @within")
    end

    test "@decreases" do
      defmodule TestError do
        use Corsa
        @decreases 1
      end
      |> test_error(Corsa.DecreasesError, "syntax error in @decreases")
    end

    test "@expects" do
      defmodule TestError do
        use Corsa

        def f() do
          @expects 1
        end
      end
      |> test_error(Corsa.ExpectsError, "syntax error in @expects")
    end
  end

  describe "mulitple definitions" do
    test "@pre" do
      defmodule TestError do
        use Corsa
        @pre f(), do: true
        @pre f(), do: true
        def f(), do: :ok
      end
      |> test_error(Corsa.PreError, "@pre for function 'f/0' already defined")
    end

    test "@post" do
      defmodule TestError do
        use Corsa
        @post f(), do: true
        @post f(), do: true
        def f(), do: :ok
      end
      |> test_error(Corsa.PostError, "@post for function 'f/0' already defined")
    end

    test "@throws" do
      defmodule TestError do
        use Corsa
        @throws f(), do: false
        @throws f(), do: false
        def f(), do: :ok
      end
      |> test_error(Corsa.ThrowsError, "@throws for function 'f/0' already defined")
    end

    test "@within" do
      defmodule TestError do
        use Corsa
        @within f(), do: nil
        @within f(), do: nil
        def f(), do: :ok
      end
      |> test_error(Corsa.WithinError, "@within for function 'f/0' already defined")
    end

    test "@decreases" do
      defmodule TestError do
        use Corsa
        @decreases f(), do: nil
        @decreases f(), do: nil
        def f(), do: :ok
      end
      |> test_error(Corsa.DecreasesError, "@decreases for function 'f/0' already defined")
    end
  end

  describe "wrong arguments" do
    test "@pre with _" do
      defmodule TestError do
        use Corsa
        @pre id(_), do: true
        def id(x), do: x
      end
      |> test_error(Corsa.PreError, "arguments in @pre cannot be ignored with _ ")
    end

    test "@pre with repeated argument" do
      defmodule TestError do
        use Corsa
        @pre f(x, x), do: true
        def f(x, x), do: x
      end
      |> test_error(Corsa.PreError, "arguments in @pre should contain different names")
    end

    test "@post with _" do
      defmodule TestError do
        use Corsa
        @post id(_), do: true
        def id(x), do: x
      end
      |> test_error(Corsa.PostError, "arguments in @post cannot be ignored with _ ")
    end

    test "@post with result" do
      defmodule TestError do
        use Corsa
        @post id(result), do: true
        def id(x), do: x
      end
      |> test_error(Corsa.PostError, "result cannot be an argument in @post")
    end

    test "@post with repeated argument" do
      defmodule TestError do
        use Corsa
        @post f(x, x), do: true
        def f(x, x), do: x
      end
      |> test_error(Corsa.PostError, "arguments in @post should contain different names")
    end

    test "@throws with _" do
      defmodule TestError do
        use Corsa
        @throws id(_), do: nil
        def id(x), do: x
      end
      |> test_error(Corsa.ThrowsError, "arguments in @throws cannot be ignored with _ ")
    end

    test "@throws with kind" do
      defmodule TestError do
        use Corsa
        @throws id(kind), do: true
        def id(x), do: x
      end
      |> test_error(Corsa.ThrowsError, "'kind' cannot be an argument in @throws")
    end

    test "@throws with value" do
      defmodule TestError do
        use Corsa
        @throws id(value), do: true
        def id(x), do: x
      end
      |> test_error(Corsa.ThrowsError, "'value' cannot be an argument in @throws")
    end

    test "@throws with repeated argument" do
      defmodule TestError do
        use Corsa
        @throws f(x, x), do: true
        def f(x, x), do: x
      end
      |> test_error(Corsa.ThrowsError, "arguments in @throws should contain different names")
    end

    test "@within with _" do
      defmodule TestError do
        use Corsa
        @within id(_), do: nil
        def id(x), do: x
      end
      |> test_error(Corsa.WithinError, "arguments in @within cannot be ignored with _ ")
    end

    test "@within with repeated argument" do
      defmodule TestError do
        use Corsa
        @within f(x, x), do: true
        def f(x, x), do: x
      end
      |> test_error(Corsa.WithinError, "arguments in @within should contain different names")
    end

    test "@decreases with _" do
      defmodule TestError do
        use Corsa
        @decreases id(_), do: true
        def id(x), do: x
      end
      |> test_error(Corsa.DecreasesError, "arguments in @decreases cannot be ignored with _ ")
    end

    test "@decreases with repeated argument" do
      defmodule TestError do
        use Corsa
        @decreases f(x, x), do: true
        def f(x, x), do: x
      end
      |> test_error(Corsa.DecreasesError, "arguments in @decreases should contain different names")
    end

    test "@spec with repeated argument" do
      defmodule TestError do
        use Corsa
        @spec f(x :: integer(), x :: integer()) :: integer()
        def f(x, x), do: x
      end
      |> test_error(Corsa.SpecError, "arguments in @spec should contain different names")
    end
  end

  describe "exceptions in contracts" do
    test "@pre" do
      defmodule PreError do
        use Corsa
        @pre f(), do: raise("error")
        def f(), do: :ok
      end

      message =
        "@pre has incorrectly raised an exception; @pre code should never raise an exception. Exception 'RuntimeError' with value 'error' in call 'CorsaTest.PreError.f()'"

      assert_raise Corsa.PreError, message, fn -> PreError.f() end
    end

    test "@post" do
      defmodule PostError do
        use Corsa
        @post f(), do: raise("error")
        def f(), do: :ok
      end

      message =
        "@post has incorrectly raised an exception; @post code should never raise an exception. Exception 'RuntimeError' with value 'error' in call 'CorsaTest.PostError.f()'"

      assert_raise Corsa.PostError, message, fn -> PostError.f() end
    end

    test "@throws" do
      defmodule ThrowsError do
        use Corsa
        @throws f(), do: raise("error")
        def f(), do: raise("error")
      end

      message =
        "@throws has incorrectly raised an exception; @throws code should never raise an exception. Exception 'RuntimeError' with value 'error' in call 'CorsaTest.ThrowsError.f()'"

      assert_raise Corsa.ThrowsError, message, fn -> ThrowsError.f() end
    end

    test "@within" do
      defmodule WithinError do
        use Corsa
        @within f(), do: raise("error")
        def f(), do: :ok
      end

      message =
        "@within has incorrectly raised an exception; @within code should never raise an exception. Exception ':error' with value 'error' in call 'CorsaTest.WithinError.f()'"

      assert_raise Corsa.WithinError, message, fn -> WithinError.f() end
    end

    test "@decreases" do
      defmodule DecreasesError do
        use Corsa
        @decreases f(), do: raise("error")
        def f(), do: f()
      end

      message =
        "@decreases has incorrectly raised an exception; @decreases code should never raise an exception. Exception 'RuntimeError' with value 'error' in call 'CorsaTest.DecreasesError.f()'"

      assert_raise Corsa.DecreasesError, message, fn -> DecreasesError.f() end
    end

    test "@assert" do
      defmodule AssertError do
        use Corsa

        def f do
          @assert raise("error")
        end
      end

      message =
        "@assert has incorrectly raised an exception; @assert code should never raise an exception. Exception 'RuntimeError' with value 'error' in expression 'raise \"error\"'"

      assert_raise Corsa.AssertError, message, fn -> AssertError.f() end
    end

    test "@expects" do
      defmodule ExpectsError do
        use Corsa

        def f do
          @expects raise("error"), receive do
            x -> x
          end
        end
      end

      send(self(), :test)

      message =
        "@expects has incorrectly raised an exception; @expects code should never raise an exception. Exception 'RuntimeError' with value 'error' in call 'raise \"error\"'"

      assert_raise Corsa.ExpectsError, message, fn -> ExpectsError.f() end
    end
  end

  describe "guards are not supported" do
    test "@pre" do
      defmodule TestError do
        use Corsa
        @pre f(x) when is_map(x), do: true
      end
      |> test_error(Corsa.PreError, "guards are not supported in @pre")
    end

    test "@post" do
      defmodule TestError do
        use Corsa
        @post f(x) when is_map(x), do: true
      end
      |> test_error(Corsa.PostError, "guards are not supported in @post")
    end

    test "@throws" do
      defmodule TestError do
        use Corsa
        @throws f(x) when is_map(x), do: true
      end
      |> test_error(Corsa.ThrowsError, "guards are not supported in @throws")
    end

    test "@within" do
      defmodule TestError do
        use Corsa
        @within f(x) when is_map(x), do: true
      end
      |> test_error(Corsa.WithinError, "guards are not supported in @within")
    end

    test "@decreases" do
      defmodule TestError do
        use Corsa
        @decreases f(x) when is_map(x), do: true
      end
      |> test_error(Corsa.DecreasesError, "guards are not supported in @decreases")
    end
  end

  describe "warnings in contracts" do
    test "one variable overriden in @expects" do
      warnings =
        capture_io(:stderr, fn ->
          defmodule ExpectsWarnings do
            use Corsa

            def f(state) do
              @expects state > 0, receive do
                {state, x} -> x
              end
            end
          end
        end)

      assert warnings =~ "Variable \"state\" is overriden inside pattern in @expects contract"
      clean(ExpectsWarnings)
    end

    test "more than one variable overriden in expects" do
      warnings =
        capture_io(:stderr, fn ->
          defmodule ExpectsWarnings do
            use Corsa

            def f(state, x) do
              @expects state > 0, receive do
                {state, x} -> x
              end
            end
          end
        end)

      assert warnings =~
               "Variables \"state\", \"x\" are overriden inside pattern in @expects contract"

      clean(ExpectsWarnings)
    end
  end
end
