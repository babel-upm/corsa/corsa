defmodule CorsaExceptionTest do
  # import TestHelpers
  use ExUnit.Case

  describe "basic functionality" do
    defmodule Basic do
      use Corsa
      @pre f(x), do: x >= 1
      @post f(x), do: result == 2
      @throws f(x), do: x == 4
      @within f(x), do: 5000
      @spec f(integer()) :: integer() | no_return()
      def f(1), do: 2
      def f(2), do: 3
      def f(3), do: 2
      def f(4), do: raise("some error")
      def f(5), do: raise("some error")

      def f(6) do
        Process.sleep(900)
        2
      end

      def f(7) do
        Process.sleep(6000)
        2
      end

      def f(9) do
        f(10)
      end

      def f(20) do
        :ok
      end
    end

    test "@pre violation" do
      error = "@pre does not hold in call 'CorsaExceptionTest.Basic.f(0)'"
      assert_raise Corsa.PreViolationError, error, fn -> Basic.f(0) end
    end

    test "@pre satisfied" do
      assert Basic.f(1) == 2
    end

    test "@post violation" do
      error = "@post does not hold in call 'CorsaExceptionTest.Basic.f(2)' with result '3'"
      assert_raise Corsa.PostViolationError, error, fn -> Basic.f(2) end
    end

    test "@post satisfied" do
      assert Basic.f(3) == 2
    end

    test "@throws violation" do
      error = "@throws does not hold in call 'CorsaExceptionTest.Basic.f(5)'"
      assert_raise Corsa.ThrowsViolationError, error, fn -> Basic.f(5) end
    end

    test "@throws satisfied" do
      assert_raise RuntimeError, "some error", fn -> Basic.f(4) end
    end

    test "@within satisfied" do
      assert Basic.f(6)
    end

    test "@within violation" do
      Process.flag(:trap_exit, true)
      Basic.f(7)
      assert_receive({:EXIT, _pid, %Corsa.WithinViolationError{}})
      Process.flag(:trap_exit, false)
    end

    test "@spec arg violation" do
      error = "@spec does not hold in arguments in call 'CorsaExceptionTest.Basic.f(:ok)'"
      assert_raise Corsa.SpecArgViolationError, error, fn -> Basic.f(:ok) end
    end

    test "@spec result violation" do
      error = "@spec does not hold in call 'CorsaExceptionTest.Basic.f(20)' with result ':ok'"
      assert_raise Corsa.SpecResultViolationError, error, fn -> Basic.f(20) end
    end
  end

  describe "recursive function" do
    defmodule Recursive do
      use Corsa
      @pre f(n), do: n >= 0
      @post f(n), do: result <= n
      @throws f(n), do: n < 10 or n >= 15
      @within f(n), do: 1000
      @spec f(integer()) :: integer()
      def f(0), do: 0
      def f(3), do: 4
      def f(7), do: 7
      def f(10), do: raise("some error")
      def f(15), do: raise("some error")
      def f(100), do: 100
      def f(300), do: f(:ok)
      def f(400), do: :ok
      def f(n), do: f(n - 1)
    end

    test "@pre violation" do
      error = "@pre does not hold in call 'CorsaExceptionTest.Recursive.f(-1)'"
      assert_raise Corsa.PreViolationError, error, fn -> Recursive.f(-1) end
    end

    test "@pre satisfied" do
      assert Recursive.f(2) == 0
    end

    test "@post violation" do
      error = "@post does not hold in call 'CorsaExceptionTest.Recursive.f(3)' with result '4'"
      assert_raise Corsa.PostViolationError, error, fn -> Recursive.f(5) end
    end

    test "@post satisfied" do
      assert Recursive.f(9) == 7
    end

    test "@throws violation" do
      error = "@throws does not hold in call 'CorsaExceptionTest.Recursive.f(10)'"
      assert_raise Corsa.ThrowsViolationError, error, fn -> Recursive.f(14) end
    end

    test "@throws satisfied" do
      assert_raise RuntimeError, "some error", fn -> Recursive.f(20) end
    end

    # WARNING: this test may fail depending on the speed of the machine executing it
    # test "@within violation" do
    #   Process.flag(:trap_exit, true)
    #   Recursive.f(800_000)
    #   assert_receive({:EXIT, _pid, %Corsa.WithinViolationError{}})
    #   Process.flag(:trap_exit, false)
    # end

    test "@spec arg violtation" do
      error = "@spec does not hold in arguments in call 'CorsaExceptionTest.Recursive.f(:ok)'"
      assert_raise Corsa.SpecArgViolationError, error, fn -> Recursive.f(399) end
    end

    test "@spec result violation" do
      error = "@spec does not hold in call 'CorsaExceptionTest.Recursive.f(400)' with result ':ok'"
      assert_raise Corsa.SpecResultViolationError, error, fn -> Recursive.f(500) end
    end

    test "@spec satisfied" do
      Recursive.f(200)
    end
  end

  describe "assert test" do
    defmodule Example do
      use Corsa

      def f(x) do
        @assert x > 0
        :ok
      end
    end

    test "@assert violation" do
      message = "@assert does not hold in expression 'x > 0' with values: 'x = 0'"
      assert_raise Corsa.AssertViolationError, message, fn -> Example.f(0) end
    end

    test "@assert satisfied" do
      assert Example.f(1) == :ok
    end
  end

  describe "expects test" do
    defmodule Counter do
      use Corsa

      def counter(state) do
        @expects v > 0, receive do
          :inc -> counter(state + 1)
          :dec -> counter(state - 1)
          {:set, v} -> counter(v)
        end
      end
    end

    test "@expects violation" do
      Process.flag(:trap_exit, true)
      send(self(), {:set, -10})
      message = "@expects does not hold in expression 'v > 0' with values: 'v = -10'"
      assert_raise Corsa.ExpectsViolationError, message, fn -> Counter.counter(0) end
      Process.flag(:trap_exit, false)
    end

    test "@expects satisfied" do
      pid = spawn(fn -> Counter.counter(0) end)
      send(pid, {:set, 10})
    end
  end

  describe "decreases test" do
    defmodule Test do
      use Corsa

      @pre fact(n), do: n >= 0
      @decreases fact(n), do: n
      def fact(0), do: 1
      def fact(n), do: n * fact(n - 1)

      @pre qsort(l), do: is_list(l)
      @decreases qsort(l), do: length(l)
      def qsort([]), do: []

      def qsort([h | tl]) do
        l1 = qsort(Enum.filter(tl, fn x -> x <= h end))
        l2 = [h]
        l3 = qsort(Enum.filter(tl, fn x -> x > h end))
        l1 ++ l2 ++ l3
      end

      @pre bad_fact(n), do: n >= 0
      @decreases bad_fact(n), do: n
      def bad_fact(0), do: 1
      def bad_fact(5), do: bad_fact(6)
      def bad_fact(n), do: n * bad_fact(n - 1)
    end

    test "@decreases violation" do
      message =
        "@decreases does not hold, call 'CorsaExceptionTest.Test.bad_fact(6)' does not decrease after 'CorsaExceptionTest.Test.bad_fact(5)'"

      assert_raise Corsa.DecreasesViolationError, message, fn -> Test.bad_fact(10) end
    end

    test "@decreases satisfied" do
      3_628_800 = Test.fact(10)
      [2, 3, 5, 7, 11, 13, 17, 19] = Test.qsort([3, 7, 11, 17, 5, 2, 13, 19])
    end
  end
end
