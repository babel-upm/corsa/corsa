ExUnit.start()

defmodule TestHelpers do
  use ExUnit.Case
  import ExUnit.CaptureLog

  def eval(code), do: code |> Macro.to_string() |> Code.eval_string()

  def test_log(test, message) do
    trace = capture_log(test)
    # IO.puts(trace)
    assert trace =~ message
  end

  defmacro test_error(code, error, message) do
    quote do
      assert_raise unquote(error), unquote(message), fn -> eval(unquote(code)) end
    end
  end

  def clean(module) do
    :code.purge(module)
    :code.delete(module)
    :ok
  end
end
