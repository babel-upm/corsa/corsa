defmodule CorsaDefpLogsTest do
  import TestHelpers
  use ExUnit.Case

  describe "basic functionality" do
    defmodule Basic do
      use Corsa, logger: :debug
      @pre g(x), do: x >= 1
      @post g(x), do: result == 2
      @throws g(x), do: x == 4
      @within g(x), do: 5000
      @spec g(integer()) :: integer() | no_return()
      defp g(1), do: 2
      defp g(2), do: 3
      defp g(3), do: 2
      defp g(4), do: raise("some error")
      defp g(5), do: raise("some error")

      defp g(6) do
        Process.sleep(900)
        2
      end

      defp g(7) do
        Process.sleep(6000)
        2
      end

      defp g(9) do
        g(10)
      end

      defp g(20) do
        :ok
      end

      defp g(x) do
        x
      end

      def f(x), do: g(x)
    end

    test "@pre violation" do
      fn -> Basic.f(0) end
      |> test_log("@pre does not hold in call 'CorsaDefpLogsTest.Basic.g(0)'")
    end

    test "@pre satisfied" do
      assert Basic.f(1) == 2
    end

    test "@post violation" do
      fn -> Basic.f(2) end
      |> test_log("@post does not hold in call 'CorsaDefpLogsTest.Basic.g(2)' with result '3'")
    end

    test "@post satisfied" do
      assert Basic.f(3) == 2
    end

    test "@throws violation" do
      fn ->
        try do
          Basic.f(5)
        catch
          _, _ -> :ok
        end
      end
      |> test_log("@throws does not hold in call 'CorsaDefpLogsTest.Basic.g(5)'")
    end

    test "@throws satisfied" do
      assert_raise RuntimeError, "some error", fn -> Basic.f(4) end
    end

    test "@within satisfied" do
      assert Basic.f(6)
    end

    # test "@within violation" do
    #   Process.flag(:trap_exit, true)
    #   Basic.f(7)
    #   assert_receive({:EXIT, _pid, %Corsa.WithinViolationError{}})
    #   Process.flag(:trap_exit, false)
    # end

    test "@spec arg violation" do
      fn -> Basic.f(:ok) end
      |> test_log("@spec does not hold in arguments in call 'CorsaDefpLogsTest.Basic.g(:ok)'")
    end

    test "@spec result violation" do
      fn -> Basic.f(20) end
      |> test_log("@spec does not hold in call 'CorsaDefpLogsTest.Basic.g(20)' with result ':ok'")
    end
  end

  describe "recursive function" do
    defmodule Recursive do
      use Corsa, logger: :debug
      @pre g(n), do: n >= 0
      @post g(n), do: result <= n
      @throws g(n), do: n < 10 or n >= 15
      # @within g(n), do: 1000
      @spec g(integer()) :: integer()
      defp g(-10), do: -10
      defp g(0), do: 0
      defp g(3), do: 4
      defp g(7), do: 7
      defp g(10), do: raise("some error")
      defp g(15), do: raise("some error")
      defp g(100), do: 100
      defp g(300), do: g(:ok)
      defp g(400), do: :ok
      defp g(n), do: g(n - 1)

      def f(n), do: g(n)
    end

    test "@pre violation" do
      fn -> Recursive.f(-1) end
      |> test_log("@pre does not hold in call 'CorsaDefpLogsTest.Recursive.g(-1)'")
    end

    test "@pre satisfied" do
      assert Recursive.f(2) == 0
    end

    test "@post violation" do
      fn -> Recursive.f(5) end
      |> test_log("@post does not hold in call 'CorsaDefpLogsTest.Recursive.g(3)' with result '4'")
    end

    test "@post satisfied" do
      assert Recursive.f(9) == 7
    end

    test "@throws violation" do
      fn ->
        try do
          Recursive.f(14)
        catch
          _, _ -> :ok
        end
      end
      |> test_log("@throws does not hold in call 'CorsaDefpLogsTest.Recursive.g(10)'")
    end

    test "@throws satisfied" do
      assert_raise RuntimeError, "some error", fn -> Recursive.f(20) end
    end

    #   # WARNING: this test may fail depending on the speed of the machine executing it
    #   # test "@within violation" do
    #   #   Process.flag(:trap_exit, true)
    #   #   Recursive.f(800_000)
    #   #   assert_receive({:EXIT, _pid, %Corsa.WithinViolationError{}})
    #   #   Process.flag(:trap_exit, false)
    #   # end

    # test "@spec arg violtation" do
    #   fn -> Recursive.f(399) end
    #   |> test_log("@spec does not hold in arguments in call 'CorsaExceptionTest.Recursive.f(:ok)'")
    # end

    test "@spec result violation" do
      fn -> Recursive.f(500) end
      |> test_log(
        "@spec does not hold in call 'CorsaDefpLogsTest.Recursive.g(400)' with result ':ok'"
      )
    end

    test "@spec satisfied" do
      Recursive.f(200)
    end
  end

  describe "assert test" do
    defmodule Example do
      use Corsa, logger: :debug

      defp g(x) do
        @assert x > 0
        :ok
      end

      def f(x), do: g(x)
    end

    test "@assert violation" do
      fn -> Example.f(0) end
      |> test_log("@assert does not hold in expression 'x > 0' with values: 'x = 0'")
    end

    test "@assert satisfied" do
      assert Example.f(1) == :ok
    end
  end

  describe "expects test" do
    defmodule Counter do
      use Corsa, logger: :debug

      defp counterp(_state) do
        @expects v > 0, receive do
          :inc -> :ok
          :dec -> :ok
          {:set, v} -> :ok
        end
      end

      def counter(state), do: counterp(state)
    end

    test "@expects violation" do
      send(self(), {:set, -10})

      fn -> Counter.counter(0) end
      |> test_log("@expects does not hold in expression 'v > 0' with values: 'v = -10'")
    end

    test "@expects satisfied" do
      pid = spawn(fn -> Counter.counter(0) end)
      send(pid, {:set, 10})
    end
  end

  describe "decreases test" do
    defmodule Test do
      use Corsa, logger: :debug

      @pre factp(n), do: n >= 0
      @decreases factp(n), do: n
      defp factp(0), do: 1
      defp factp(n), do: n * factp(n - 1)

      def fact(n), do: factp(n)

      @pre qsortp(l), do: is_list(l)
      @decreases qsortp(l), do: length(l)
      defp qsortp([]), do: []

      defp qsortp([h | tl]) do
        l1 = qsort(Enum.filter(tl, fn x -> x <= h end))
        l2 = [h]
        l3 = qsort(Enum.filter(tl, fn x -> x > h end))
        l1 ++ l2 ++ l3
      end

      def qsort(l), do: qsortp(l)

      @pre bad_factp(n), do: n >= 0
      @decreases bad_factp(n), do: n
      defp bad_factp(0), do: 1
      defp bad_factp(5), do: bad_fact(10)
      defp bad_factp(10), do: 1
      defp bad_factp(n), do: n * bad_fact(n - 1)

      def bad_fact(n), do: bad_factp(n)
    end

    test "@decreases violation" do
      fn -> Test.bad_fact(9) end
      |> test_log(
        "@decreases does not hold, call 'CorsaDefpLogsTest.Test.bad_factp(10)' does not decrease after 'CorsaDefpLogsTest.Test.bad_factp(5)'"
      )
    end

    test "@decreases satisfied" do
      3_628_800 = Test.fact(10)
      [2, 3, 5, 7, 11, 13, 17, 19] = Test.qsort([3, 7, 11, 17, 5, 2, 13, 19])
    end
  end
end
