defmodule PolynomialTest do
  use ExUnit.Case

  test "polynomial0" do
    error = ~r/^@pre does not hold in call 'Polynomials.plus(.*)'/

    assert_raise Corsa.PreViolationError, error, fn ->
      Polynomials.plus({[{4, 0}], :x}, {[{3, 0}], :y})
    end
  end

  test "polynomial1" do
    assert Polynomials.plus({[{4, 0}], :x}, {[{3, 0}], :x}) == {[{7, 0}], :x}
  end

  test "polynomial2" do
    error = ~r/^@spec does not hold in arguments in call 'Polynomials.plus(.*)'/
    assert_raise Corsa.SpecArgViolationError, error, fn -> Polynomials.plus(0, 1) end
  end
end
