defmodule Polynomials do
  use Corsa

  @moduledoc false

  @spec plus(Polynomial.polynomial(), Polynomial.polynomial()) :: Polynomial.polynomial()
  @pre plus(p1, p2), do: Polynomial.variable(p1) == Polynomial.variable(p2)
  @post plus(p1, p2), do: Polynomial.variable(p1) == Polynomial.variable(result)
  def plus({p1, v}, {p2, _}), do: {pplus(p1, p2), v}
  def pplus(p1, []), do: p1
  def pplus([], p2), do: p2

  def pplus(p1 = [{fc1, exp1} | rest1], p2 = [{fc2, exp2} | rest2]) do
    cond do
      exp1 > exp2 ->
        [{fc1, exp1} | pplus(rest1, p2)]

      exp2 > exp1 ->
        [{fc2, exp2} | pplus(rest2, p1)]

      true ->
        [{fc1 + fc2, exp1} | pplus(rest1, rest2)]
    end
  end
end
