defmodule Polynomial do
  use Corsa

  @moduledoc false

  # Note: we are using qualified function names, otherwise things do not work well in the user
  # of the type!
  @type! polynomial :: {[{integer, integer}], atom} when Polynomial.is_polynomial(polynomial)

  # Note: we are using qualified function names, otherwise things do not work well in the user
  # of the type!
  def is_polynomial({[], v}), do: is_atom(v)

  def is_polynomial({[first | rest], v}) do
    is_atom(v) and is_well_formed(first) and is_polynomial(rest, first)
  end

  def is_polynomial(_), do: false

  def is_well_formed({constant, exponent}) do
    is_integer(constant) and is_integer(exponent) and constant > 0 and exponent >= 0
  end

  def is_well_formed(_), do: false

  def is_polynomial([], _), do: true

  def is_polynomial([first | rest], {_, exponent}) do
    is_well_formed(first) and
      case first do
        {_, first_exponent} -> exponent > first_exponent and is_polynomial(rest, first)
      end
  end

  def variable({_, var}), do: var
end
