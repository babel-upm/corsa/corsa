locals_without_parens = [
  pre: 2,
  post: 2,
  throws: 2,
  within: 2,
  expects: 2,
  decreases: 2
]

[
  inputs: [
    "{mix,.iex,.formatter,.credo}.exs",
    "{config,lib,test}/**/*.{ex,exs}"
  ],
  line_length: 100,
  locals_without_parens: locals_without_parens,
  export: [locals_without_parens: locals_without_parens]
]
