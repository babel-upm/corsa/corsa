################################################################################
# Assert
################################################################################

defmodule Corsa.AssertViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{expr: expr, vars: vars}) do
    expr = Macro.to_string(expr)

    values =
      Enum.map_join(vars, ", ", fn {key, value} ->
        "'#{Atom.to_string(key)} = #{inspect(value)}'"
      end)
      |> then(&if &1 == "", do: "", else: "with values: #{&1}")

    %__MODULE__{message: "@assert does not hold in expression '#{expr}' #{values}"}
  end

  def exception(_) do
    %__MODULE__{message: "@assert does not hold"}
  end
end

defmodule Corsa.AssertError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{expr: expr, kind: kind, reason: reason}) do
    expr = Macro.to_string(expr)
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@assert has incorrectly raised an exception; @assert code should never raise an exception. Exception '#{kind}' with value '#{reason}' in expression '#{expr}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Pre
################################################################################

defmodule Corsa.PreViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call}) do
    %__MODULE__{message: "@pre does not hold in call '#{call}'"}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

defmodule Corsa.PreError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, kind: kind, reason: reason}) do
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@pre has incorrectly raised an exception; @pre code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Post
################################################################################

defmodule Corsa.PostViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, result: result}) do
    %__MODULE__{message: "@post does not hold in call '#{call}' with result '#{inspect(result)}'"}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

defmodule Corsa.PostError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, kind: kind, reason: reason}) do
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@post has incorrectly raised an exception; @post code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Throws
################################################################################

defmodule Corsa.ThrowsViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call}) do
    %__MODULE__{message: "@throws does not hold in call '#{call}'"}
  end
end

defmodule Corsa.ThrowsError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, kind: kind, reason: reason}) do
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@throws has incorrectly raised an exception; @throws code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Within
################################################################################

defmodule Corsa.WithinViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(error) do
    call = error[:call]
    %__MODULE__{message: "@within does not hold in call '#{call}'"}
  end
end

defmodule Corsa.WithinError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, kind: kind, reason: reason}) do
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@within has incorrectly raised an exception; @within code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Decreases
################################################################################

defmodule Corsa.DecreasesViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{old_call: old_call, call: call}) do
    %__MODULE__{
      message: "@decreases does not hold, call '#{call}' does not decrease after '#{old_call}'"
    }
  end
end

defmodule Corsa.DecreasesError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, kind: kind, reason: reason}) do
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@decreases has incorrectly raised an exception; @decreases code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Expects
################################################################################

defmodule Corsa.ExpectsViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{expr: expr, vars: vars}) do
    expr = Macro.to_string(expr)

    values =
      Enum.map_join(vars, ", ", fn {key, value} ->
        "'#{Atom.to_string(key)} = #{inspect(value)}'"
      end)
      |> then(&if &1 == "", do: "", else: "with values: #{&1}")

    %__MODULE__{message: "@expects does not hold in expression '#{expr}' #{values}"}
  end
end

defmodule Corsa.ExpectsError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{expr: call, kind: kind, reason: reason}) do
    call = Macro.to_string(call)
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@expects has incorrectly raised an exception; @expects code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end

################################################################################
# Spec
################################################################################

defmodule Corsa.SpecArgViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call}) do
    %__MODULE__{
      message: "@spec does not hold in arguments in call '#{call}'"
    }
  end
end

defmodule Corsa.SpecResultViolationError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{call: call, result: result}) do
    %__MODULE__{
      message: "@spec does not hold in call '#{call}' with result '#{inspect(result)}'"
    }
  end
end

defmodule Corsa.SpecError do
  defexception [:message, corsa_exception: true]

  @impl true
  def exception(%{expr: call, kind: kind, reason: reason}) do
    call = Macro.to_string(call)
    kind = inspect(kind)
    reason = if is_exception(reason), do: Exception.message(reason), else: reason

    message =
      "@spec has incorrectly raised an exception; @spec code should never raise an exception. Exception '#{kind}' with value '#{reason}' in call '#{call}'"

    %__MODULE__{message: message}
  end

  def exception(error) do
    %__MODULE__{message: error}
  end
end
